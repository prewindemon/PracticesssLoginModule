# PracticesssLoginModule

[![CI Status](https://img.shields.io/travis/prewindemon/PracticesssLoginModule.svg?style=flat)](https://travis-ci.org/prewindemon/PracticesssLoginModule)
[![Version](https://img.shields.io/cocoapods/v/PracticesssLoginModule.svg?style=flat)](https://cocoapods.org/pods/PracticesssLoginModule)
[![License](https://img.shields.io/cocoapods/l/PracticesssLoginModule.svg?style=flat)](https://cocoapods.org/pods/PracticesssLoginModule)
[![Platform](https://img.shields.io/cocoapods/p/PracticesssLoginModule.svg?style=flat)](https://cocoapods.org/pods/PracticesssLoginModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PracticesssLoginModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PracticesssLoginModule'
```

## Author

prewindemon, 121555495@qq.com

## License

PracticesssLoginModule is available under the MIT license. See the LICENSE file for more info.
