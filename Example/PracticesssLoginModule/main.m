//
//  main.m
//  PracticesssLoginModule
//
//  Created by prewindemon on 06/11/2018.
//  Copyright (c) 2018 prewindemon. All rights reserved.
//

@import UIKit;
#import "PracticesssLoginAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PracticesssLoginAppDelegate class]));
    }
}
