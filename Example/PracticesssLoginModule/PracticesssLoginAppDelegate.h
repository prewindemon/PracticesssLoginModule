//
//  PracticesssLoginAppDelegate.h
//  PracticesssLoginModule
//
//  Created by prewindemon on 06/11/2018.
//  Copyright (c) 2018 prewindemon. All rights reserved.
//

@import UIKit;

@interface PracticesssLoginAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
